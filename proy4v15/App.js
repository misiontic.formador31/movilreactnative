/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState, useEffect} from 'react';
import Formulario from './componentes/Formulario';
import Clima from './componentes/Clima';

import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Keyboard,
  Alert
  
} from 'react-native';

const App = () => {

  const ocultarTeclado = () => {
    Keyboard.dismiss();
  }
   
  const [busqueda, guardarBusqueda] = useState(
    {ciudad:'',pais:''}
  )

  const {ciudad, pais} =busqueda;

  const [consultarAPI, guardarConsultarAPI] = useState(false);
  const [resultado, guardarResultado]=useState({})
  const [bgcolor, guardarBgcolor]=useState('rgb(71,149,212)')

    //useEffect

    useEffect(() => {
      const consultarClima = async () =>{
        if(consultarAPI){
          const appId = 'aa35a30ec93c400990f9d08bb4b3293d';
          const url= `http://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${appId}`;
          console.log(url);
          try{
            const respuesta = await fetch(url);
            const resultado = await respuesta.json();
            guardarResultado(resultado);
            guardarConsultarAPI(false);

            //modifica el valor del fondo
            const kelvin=273.15;
            const{main} = resultado;
            const actual = main.temp - kelvin;

            if(actual <10){
              guardarBgcolor('rgb(105,108,149)')
            }else if(actual>=10 && actual <25){
              guardarBgcolor('rgb(71,149,212)')
            }else{
              guardarBgcolor('rgb(178,28,61)')
            }
          }
          catch(error){
            console.log(error);
            mostrarAlerta();
          }
          
        }
        
      }
      consultarClima(); 
    }, [consultarAPI]);


    const bgcolorApp = {
      backgroundColor:bgcolor
    }
    const mostrarAlerta=() =>{
      Alert.alert(
        'Error',
        'No hay resultados, intenta con otra ciudad o país',
        [{text:'Ok'}]
    )
    return;
    }
  return (
    <>
      <TouchableWithoutFeedback onPress={() => ocultarTeclado()}>
        <View style={[styles.app, bgcolorApp]}>
          <View style={styles.contenido}>
            <Clima
              resultado={resultado}
            />
            <Formulario 
              busqueda={busqueda}
              guardarBusqueda={guardarBusqueda}
              guardarConsultarAPI={guardarConsultarAPI}
            />

          </View>
        </View>
      </TouchableWithoutFeedback>
    </>
  );
};

const styles = StyleSheet.create({
  app: {
    flex: 1,
    
    justifyContent: 'center',
  },
  contenido: {
    marginHorizontal: '2.5%',
  },
});

export default App;
