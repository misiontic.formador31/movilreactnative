import React, {useState} from 'react';

import {
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableWithoutFeedback,
  Animated,
  Alert
} from 'react-native';

import {Picker} from '@react-native-picker/picker';

const Formulario = ({busqueda, guardarBusqueda, guardarConsultarAPI}) => {

    const {pais, ciudad} = busqueda;

    //useState
    const [animacionBoton] = useState(new Animated.Value(1));
    

    //funciones 
    const animacionEntrada=()=>{
        Animated.spring(animacionBoton,{
            toValue:.75,
            useNativeDriver:true

            
        }).start();
    }

    const animacionSalida=()=>{
        Animated.spring(animacionBoton,{
            toValue:1,
            useNativeDriver:true,
            friccion:4,
            tension:90,
        }).start();
    }

    const consultarClima=()=>{
        if(pais.trim()===''|| ciudad.trim()===''){
            Alert.alert(
                'Error',
                'Agregar una ciudad y país',
                [{text:'Entendido'}]
            )
            return;
        }
        //consulta en la API
        guardarConsultarAPI(true);

    }

    const estiloAnimacion = {
        transform:[{scale:animacionBoton}]
    }

  return (
    <>
      <View>
        <View>
          <TextInput
            value={ciudad}
            onChangeText={ciudad=>guardarBusqueda({...busqueda,ciudad})}
            style={styles.input}
            placeholder="Ciudad"
            placeholderTextColor="#666"
          />

          <Picker 
          itemStyle={{height: 120, backgroundColor: '#FFF'}}
          onValueChange={pais=>guardarBusqueda({...busqueda,pais})}
          selectedValue={pais}
          >
            <Picker.Item label="--- Seleccione un país ---" value="" />
            <Picker.Item label="--- Estados Unidos ---" value="US" />
            <Picker.Item label="--- México ---" value="MX" />
            <Picker.Item label="--- Argentina ---" value="AR" />
            <Picker.Item label="--- Colombia ---" value="CO" />
            <Picker.Item label="--- Costa Rica ---" value="CR" />
            <Picker.Item label="--- España ---" value="ES" />
            <Picker.Item label="--- Perú ---" value="PE" />
          </Picker>
        </View>
        <TouchableWithoutFeedback
        
        onPressIn={()=> animacionEntrada()}
        onPressOut={()=>animacionSalida()}
        onPress={() => consultarClima()}
        >

          <Animated.View style={[styles.btnBuscar, estiloAnimacion]}>
            <Text style={styles.textoBuscar}>Buscar Clima</Text>
          </Animated.View>
        </TouchableWithoutFeedback>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  formulario: {
    marginTop: 100,
  },
  input: {
    padding: 10,
    height: 50,
    backgroundColor: '#FFF',
    fontSize: 20,
    marginBottom: 20,
    textAlign: 'center',
  },
  btnBuscar: {
    marginTop: 50,
    backgroundColor: '#000',
    padding: 10,
    justifyContent: 'center',
  },
  textoBuscar: {
    color: '#FFF',
    textTransform: 'uppercase',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize:18
  },
});

export default Formulario;
