import {initializeApp} from 'firebase/app';
import {collection, getDocs, initializeFirestore} from 'firebase/firestore';

import firebaseConfig from './config';

class Firebase {
  constructor() {
    const app = initializeApp(firebaseConfig);
    this.db = initializeFirestore(app, {
      experimentalAutoDetectLongPolling: true,
    });
    this.collection = collection;
    this.getDocs = getDocs;
  }
}

const firebase = new Firebase();

export default firebase;
