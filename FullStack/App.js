import 'react-native-gesture-handler';


 import React from 'react';
 import {StyleSheet, View, Text} from 'react-native';

 //import para navegacion
 import {NavigationContainer} from '@react-navigation/native';
 import {createStackNavigator} from '@react-navigation/stack';

 //import de vistas
 import DetallePlatillo from './views/DetallePlatillo';
 import FormularioPlatillo from './views/FormularioPlatillo';
 import Menu from './views/Menu';
 import NuevaOrden from './views/NuevaOrden';
 import ProgresoPedido from './views/ProgresoPedido';
 import ResumenPedido from './views/ResumenPedido';

//import state de context
import FirebaseState from './context/firebase/firebaseState';
import PedidosState from './context/pedidos/pedidosState';
import PedidosContext from './context/pedidos/pedidosContext';

  const Stack = createStackNavigator();


 const App = () => {
   return (<>

      <NavigationContainer>
        <FirebaseState>
        <PedidosState>
        <Stack.Navigator
          initialRouteName='NuevaOrden'
          screenOptions={{
            headerStyle:{
              backgroundColor:'#FFDA00'
            },
            headerTitleStyle:{
              fontWeight:'bold'
            }



          }}
        >
          <Stack.Screen
          name="NuevaOrden"
          component={NuevaOrden}
          options={{
            title: "Nueva Orden"
          }}
          />

          <Stack.Screen
          name="DetallePlatillo"
          component={DetallePlatillo}
          options={{
            title: "Detalle Platillo"
          }}
          />

<Stack.Screen
          name="FormularioPlatillo"
          component={FormularioPlatillo}
          options={{
            title: "Formulario Platillo"
          }}
          />

<Stack.Screen
          name="Menu"
          component={Menu}
          options={{
            title: "Menu"
          }}
          />



<Stack.Screen
          name="ProgresoPedido"
          component={ProgresoPedido}
          options={{
            title: "Progreso Pedido"
          }}
          />

<Stack.Screen
          name="ResumenPedido"
          component={ResumenPedido}
          options={{
            title: "Resumen Pedido"
          }}
          />
        </Stack.Navigator>
        </PedidosState>
      </FirebaseState>
      </NavigationContainer>
      
   
   </>);
 };
 
 const styles = StyleSheet.create({});
 
 export default App;