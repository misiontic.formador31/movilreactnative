import React, {useReducer} from 'react';

import firebase from '../../firebase/index';

import pedidosReducer from './pedidosReducer';
import pedidosContext from './pedidosContext';
import PedidosContext from './pedidosContext';

const PedidoState = props =>{

    console.log(firebase);
    //state inicial
    const initialState = {
        pedido: []
    }

    // useReducer con dispatch para ejecutar las funciones

    const [state, dispatch] = useReducer(pedidosReducer, initialState);
    return(
        <PedidosContext.Provider
        value={{
            pedido:state.pedido,
            
        }}
        >
            {props.children}
        </PedidosContext.Provider>
    )
}

export default PedidoState;