import React, {useReducer} from 'react';

import firebaseReducer from './firebaseReducer';
import FirebaseContext from './firebaseContext';
import firebase from '../../firebase';

import {OBTENER_PRODUCTOS} from '../../types';

const FirebaseState = props => {
  const initialState = {
    menu: [],
  };

  const [state, dispatch] = useReducer(firebaseReducer, initialState);

  const obtenerProductos = async () => {
    try {
      const querySnapshot = await firebase.getDocs(
        firebase.collection(firebase.db, 'productos'),
      );
  
      let platillos = [];
      querySnapshot.forEach(doc => {
        platillos.push({id: doc.id, ...doc.data()});
      });

      console.log(platillos);

      dispatch({type: OBTENER_PRODUCTOS, payload: platillos});
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <FirebaseContext.Provider
      value={{
        menu: state.menu,
        firebase,
        obtenerProductos,
      }}>
      {props.children}
    </FirebaseContext.Provider>
  );
};

export default FirebaseState;
