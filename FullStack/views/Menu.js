import React, {useContext, useEffect} from 'react';
import {StyleSheet, Fragment, View, Text} from 'react-native';
import FirebaseContext from '../context/firebase/firebaseContext';
import {NativeBaseProvider, List} from 'native-base';


const Menu = () => {
  const {menu, obtenerProductos} = useContext(FirebaseContext);

  useEffect(() => {
    obtenerProductos();
  }, []);

  if (menu.length === 0) return null;

  return (
    <View>
      {menu.map(platillo => {
        return <Text key={platillo.id}>{platillo.nombre}</Text>;
      })}
    </View>
  );
};

export default Menu;
