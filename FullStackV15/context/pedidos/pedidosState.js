import React, {useReducer} from 'react';

import PedidosReducer from './pedidosReducer';
import PedidosContext from './pedidosContext';

const FirebaseState = props => {

    console.log(firebase);

    const initialState={
        pedido: []
    }

    //useReducer con dispatch para ejecutar las funciones
    const [state, dispatch ] = useReducer(PedidosReducer, initialState);


    return(
        <FirebaseContext.Provider
        value={{
            pedido: state.pedido
        }}
        >
            
            {props.children}
        </FirebaseContext.Provider>
    )
}

export default FirebaseState;