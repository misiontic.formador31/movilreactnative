import {OBTENER_PRODUCTOS_EXITO} from '../../types';

const firebaseReducer = (state, action) => {
  switch (action.type) {
    case OBTENER_PRODUCTOS:
      return {...state, menu: action.payload};

    default:
      return state;
  }
};

export default firebaseReducer;
