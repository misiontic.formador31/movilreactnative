import React, {useReducer} from 'react';

import firebase from '../../firebase';
import firebaseReducer from './firebaseReducer';
import FirebaseContext from './firebaseContext';

import {OBTENER_PRODUCTOS} from '../../types';

const FirebaseState = props => {
  const initialState = {menu: []};

  const [state, dispatch] = useReducer(firebaseReducer, initialState);

  const obtenerProductos = async () => {
    dispatch({type: OBTENER_PRODUCTOS},menu);

    try {
      const querySnapshot = await firebase.getDocs(
        firebase.collection(firebase.db, 'productos'),
      );
      querySnapshot.forEach(doc => {
        const jsonPrint={id:doc.id,...doc.data()}
        menu=jsonPrint;
        console.log('el menu es:');
        console.log(menu);
      });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <FirebaseContext.Provider
      value={{menu: state.menu, firebase, obtenerProductos}}>
      {props.children}
    </FirebaseContext.Provider>
  );
};

export default FirebaseState;