/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState, useEffect} from 'react';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  Keyboard,
  View,
  Alert,
} from 'react-native';

import Formulario from './componentes/Formulario';
import Clima from './componentes/Clima';

const App = () => {
  const [busqueda, guardarBusqueda] = useState({
    ciudad: '',
    pais: '',
  });

  const [consultar, guardarConsultar] = useState(false);
  const [resultado, guardarResultado] =useState({});
  const [bgcolor, guardarBgcolor]=useState('rgb(71,149,212');

  const {ciudad, pais} = busqueda;

  useEffect(() => {
    const consultarClima = async () => {
      if (consultar) {
        console.log('Consultando la API....');
        const appId = 'aa35a30ec93c400990f9d08bb4b3293d';
        const url = `http://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${appId}`;

        try {
          const respuesta = await fetch(url);
          const resultado = await respuesta.json();
          console.log(resultado);
          guardarResultado(resultado);
          guardarConsultar(false);
          //modificar los colores del fondo basado en la temperatura
          const kelvin = 273.15
          const {main} = resultado;
          const actual = main.temp - kelvin;

          if(actual <12){
            guardarBgcolor('rgb(105,108,149)')
          }else if (actual >=12 && actual <20) {
            guardarBgcolor('rgb(71,149,212)')
          }else{
            guardarBgcolor('rgb(178,28,61)')
          }

        } catch (error) {
          mostrarAlerta();
        }
      }
    }
    consultarClima();
  }, [consultar]);

  const ocultarTeclado = () => {
    Keyboard.dismiss();
  };

  const bgColorApp={
    backgroundColor:bgcolor
  }

  const mostrarAlerta = () => {
    Alert.alert('Error', 'Verifica la ciudad y  el país para la busqueda', [
      {text: 'OK'},
    ]);
  };

  return (
    <>
      <TouchableWithoutFeedback onPress={() => ocultarTeclado()}>
        <View style={[styles.app, bgColorApp]}>
          <View style={styles.contenido}>
            <Clima
              resultado={resultado}
            />


            <Formulario
              busqueda={busqueda}
              guardarBusqueda={guardarBusqueda}
              guardarConsultar={guardarConsultar}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </>
  );
};

const styles = StyleSheet.create({
  app: {
    flex: 1,
    backgroundColor: 'rgb(71,149,212)',
    justifyContent: 'center',
  },
  contenido: {
    marginHorizontal: '2.5%',
  },
});

export default App;
