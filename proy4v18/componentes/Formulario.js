import React, {useState} from 'react';
import {
  Text,
  TextInput,
  View,
  TouchableWithoutFeedback,
  StyleSheet,
  Animated,
  Alert
} from 'react-native';
import {Picker} from '@react-native-picker/picker';

const Formulario = ({busqueda, guardarBusqueda, guardarConsultar}) => {
  
  //useState
  const [animacionboton] = useState(new Animated.Value(1));
  const {pais, ciudad} = busqueda;

  //Funciones
  const consultarClima = () => {
    console.log('entro');
    if(pais.trim()==='' || ciudad.trim()===''){
      mostrarAlerta();
      return;
    }
    //Tiene datos - realiza la consulta api
    guardarConsultar(true);
  }


  const mostrarAlerta= () => {
    Alert.alert(
      'Error',
      'Agrega una ciudad y país para la busqueda',
      [{text:'Entendido'}]
    )
  }
  

  const animacionEntrada = () => {
    Animated.spring(animacionboton, {
      toValue: 0.75,
      useNativeDriver:true
    }).start();
  };
  const animacionSalida = () => {
    Animated.spring(animacionboton, {
      toValue: 1, //al tamaño esperado
      useNativeDriver:true,
      friction: 1, //cuantos rebotes
      tension: 10, //suavidad
    }).start();
  };

  const estiloAnimacion = {
    transform: [{scale: animacionboton}],
  };



  return (
    <>
      <View style={styles.titulo}>
        <View>
          <TextInput
            value={ciudad}
            style={styles.input}
            onChangeText={ciudad=>guardarBusqueda({...busqueda, ciudad})}
            placeholder="Ciudad"
            placeholderTextColor="#666"
          />
        </View>

        <Picker 
        selectedValue={pais}
        itemStyle={{height: 120, backgroundColor: '#FFF'}}
        onValueChange={pais => guardarBusqueda({...busqueda, pais})}
      >
          <Picker.Item label="--- Seleccione un país ---" value="" />
          <Picker.Item label="--- Estados unidos ---" value="US" />
          <Picker.Item label="--- Mexico ---" value="MX" />
          <Picker.Item label="--- Argentina ---" value="AR" />
          <Picker.Item label="--- Colombia ---" value="CO" />
          <Picker.Item label="--- Costa Rica ---" value="CR" />
          <Picker.Item label="--- España ---" value="ES" />
          <Picker.Item label="--- Perú ---" value="PE" />
        </Picker>
        <TouchableWithoutFeedback
          onPressIn={() => animacionEntrada()}
          onPressOut={() => animacionSalida()}
          onPress={()=>consultarClima()}>
          <Animated.View style={[styles.btnBuscar, estiloAnimacion]}>
            <Text style={styles.textoBuscar}>Buscar Clima</Text>
          </Animated.View>
        </TouchableWithoutFeedback>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  titulo: {
    marginTop: 100,
  },
  input: {
    padding: 10,
    height: 50,
    backgroundColor: '#FFF',
    fontSize: 20,
    marginBottom: 20,
    textAlign: 'center',
  },
  btnBuscar: {
    marginTop: 50,
    backgroundColor: '#000',
    padding: 10,
    justifyContent: 'center',
  },
  textoBuscar: {
    color: '#FFF',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    textAlign: 'center',
    fontSize: 18,
  },
});

export default Formulario;
