import React, {useState} from 'react';
import {
  Text,
  TextInput,
  View,
  TouchableWithoutFeedback,
  StyleSheet,
  Animated,
  Alert,
  Image
} from 'react-native';

const Clima = ({resultado}) => {
  const {name, main, weather} = resultado;

  if (!name) return null;

  const kelvin = 273.15;

  return (
    <>
      <View style={styles.clima}>
        <Text style={[styles.texto, styles.actual]}>
          {parseInt(main.temp - kelvin)}
          <Text style={styles.temperatura}>&#x2103;
          </Text>
          <Image
            style={{width:66, height:58}}
            source={{uri: `http://openweathermap.org/img/wn/${resultado.weather[0].icon}@2x.png`}}

          />
        </Text>
      </View>
    </>
  );
};
const styles = StyleSheet.create({
  clima: {
    marginBottom: 10,
  },
  texto: {
    color: '#FFF',
    fontSize: 20,
    textAlign: 'center',
    marginRight: 20,
  },
  actual: {
    fontSize: 80,
    marginRight: 0,
    fontWeight: 'bold',
  },
  temperatura: {
    fontSize: 24,
    fontWeight: 'normal',
  },
});

export default Clima;
