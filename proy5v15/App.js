import 'react-native-gesture-handler';
import React from 'react';
import {StyleSheet, View} from 'react-native';

//import react navigation
import {NavigationContainer} from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack';

//import de vistas
import Inicio from './views/Inicio';
import Nosotros from './views/Nosotros';

const Stack = createStackNavigator();

const App = () => {
  return <>

  <NavigationContainer>
    <Stack.Navigator
      initialRouteName="Inicio"
      screenOptions={{
        headerTitleAlign:'center',
        headerStyle:{
          backgroundColor: '#62c6dd'}
      }}
    >
    <Stack.Screen
      name="Inicio"
      component={Inicio}
      options={{
        title:"Componente Principal",
        headerStyle:{
          backgroundColor:'#F4511E'
        },
        headerTintColor:'#fff',
        headerTitleStyle:{
          fontWeight:'bold'
        }

      }}
    />
    <Stack.Screen
      name="Nosotros"
      component={Nosotros}
      options={ ({route}) => (
        {title: route.params.clienteId}
      )
        
      }
    />
    </Stack.Navigator>
    
  </NavigationContainer>
  
  </>;
};

const styles = StyleSheet.create({});

export default App;
