/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StyleSheet, View, Image, Text, ScrollView} from 'react-native';

const App = () => {
  return (
    <>
      <ScrollView>
        <View style={{flexDirection: 'row'}}>
          <Image style={styles.banner} source={require('./img/bg.jpg')} />
        </View>

        <View style={styles.contenedor}>
          <Text style={styles.titulo}>Qué hacer en Pandemia</Text>
          <ScrollView horizontal={true}>
            <View>
              
              <Image
                style={styles.ciudad}
                source={require('./img/actividad1.jpg')}
              />
            </View>
            <View>
              
              <Image
                style={styles.ciudad}
                source={require('./img/actividad2.jpg')}
              />
            </View>
            <View>
              
              <Image
                style={styles.ciudad}
                source={require('./img/actividad3.jpg')}
              />
            </View>
            <View>
              
              <Image
                style={styles.ciudad}
                source={require('./img/actividad4.jpg')}
              />
            </View>
            <View>
              
              <Image
                style={styles.ciudad}
                source={require('./img/actividad5.jpg')}
              />
            </View>
          </ScrollView>

          <Text style={styles.titulo}>Los mejores hoteles</Text>
          <View>
            <View>
              
              <Image
                style={styles.mejores}
                source={require('./img/mejores1.jpg')}
              />
            </View>
            <View>
              
              <Image
                style={styles.mejores}
                source={require('./img/mejores2.jpg')}
              />
            </View>
            <View>
              
              <Image
                style={styles.mejores}
                source={require('./img/mejores3.jpg')}
              />
            </View>
          </View>
          <Text style={styles.titulo}>Hospedarse en La mesa</Text>

          <View style={styles.listado}>
            <View>
                <Image
                  style={styles.listadoItem}
                  source={require('./img/hospedaje1.jpg')}
                />
              </View>
              <View>
                
                <Image
                  style={styles.listadoItem}
                  source={require('./img/hospedaje2.jpg')}
                />
              </View>
              <View>
                
                <Image
                  style={styles.listadoItem}
                  source={require('./img/hospedaje3.jpg')}
                />
              </View>
              <View>
                
                <Image
                  style={styles.listadoItem}
                  source={require('./img/hospedaje4.jpg')}
                />
              </View>
          </View>
        </View>

      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  banner: {
    height: 250,
    flex: 1,
  },

  titulo: {
    fontWeight: 'bold',
    fontSize: 24,
    marginVertical: 20,
  },
  contenedor: {
    marginHorizontal: 10,
  },
  ciudad: {
    width: 250,
    height: 300,
    marginRight: 10,
  },
  mejores:{
    width:'100%',
    height:200,
    marginVertical:5

  },
  listado:{
    
    flexDirection:'row',
    flexWrap:'wrap',
    justifyContent:'space-between'
  },
  listadoItem:{
    
    flexBasis:'49%'
  }
});

export default App;
