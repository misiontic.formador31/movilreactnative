import React from 'react';
import {Route, BrowserRouter, Routes} from 'react-router-dom';

import Ordenes from './components/paginas/Ordenes';
import Menu from './components/Menu';
import NuevoPlatillo from './components/NuevoPlatillo';
import Sidebar from './components/ui/Sidebar';

function App() {
  return (
    <div>
      <Sidebar/>
      <Routes>
        <Route path="/" element={<Ordenes/>} />
        <Route path="/menu" element={<Menu/>} />
        <Route path="/nuevo-platillo" element={<NuevoPlatillo/>} />

      </Routes>
    </div>
  );
}

export default App;
