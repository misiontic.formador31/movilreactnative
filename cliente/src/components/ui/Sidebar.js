import React from 'react';

const Sidebar = () => {
    return ( 
        <div className="md:w-2/5 xl:w-1/5 bg-gray-800">
            <div className="p-6">
                <p className="uppercase text-white text-2xl">Restaurante App</p>
            </div>
        </div>
     );
}
 
export default Sidebar;