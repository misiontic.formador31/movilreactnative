/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, {useState} from 'react';
import {
  Text,
  StyleSheet,
  View,
  FlatList,
  ScrollView,
  TouchableHighlight, Platform, TouchableWithoutFeedback, Keyboard
} from 'react-native';
import Cita from './componentes/Citas';
import Formulario from './componentes/Formulario';

const App = () => {
  const [mostrarform, guardarMostrarForm] = useState(false);
  //definicion de state de citas
  const [citas, setCitas] = useState([]);

  const eliminarPaciente = id => {
    setCitas(citasActuales => {
      return citasActuales.filter(cita => cita.id !== id);
    });
  };

  //formulario mostrar o ocultar
  const mostrarFormulario = () => {
    console.log('se modifico');
    guardarMostrarForm(!mostrarform);
  };

  //cerrar teclado
  const cerrarTeclado = () => {
    Keyboard.dismiss();
  }

  return (
    <TouchableWithoutFeedback onPress={() => cerrarTeclado()}>
    <View style={styles.contenedor}>
      <Text style={styles.titulo}>Proyecto de Citas</Text>

      <View>
        <TouchableHighlight
          style={styles.btnMostrarForm}
          onPress={() => mostrarFormulario()
            
          }>
          <Text style={styles.textoMostrar}>{mostrarform? 'Cancelar crear cita':'Crear Nueva Cita' }</Text>
        </TouchableHighlight>
      </View>
          
      <View style={styles.contenido}>
        {mostrarform ? (
          <>
          <Text style={styles.titulo}>Crear Nueva Cita</Text>
          <Formulario 
            citas={citas}
            setCitas={setCitas}
            guardarMostrarForm={guardarMostrarForm}
          />
          </>
        ) : (
          <>
            <Text style={styles.titulo}>
              {citas.length > 0 ? 'Administrador de Citas' : 'No hay citas'}
            </Text>

            <FlatList
              style={styles.listado}
              data={citas}
              renderItem={({item}) => (
                <Cita item={item} eliminarPaciente={eliminarPaciente} />
              )}
              keyExtractor={cita => cita.id}
            />
          </>
        )}
      </View>
    </View>
    </TouchableWithoutFeedback>
  );
};

//Hoja de estilos
const styles = StyleSheet.create({
  contenedor: {
    backgroundColor: '#0099cc',
    minHeight: '100%',
  },

  titulo: {
    marginTop: Platform.OS=== 'ios'? 40:20,
    marginBottom: 10,
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  contenido: {
    flex: 1,
    marginHorizontal: '5%',
  },
  listado: {
    flex: 1,
  },
  btnMostrarForm: {
    padding: 10,
    backgroundColor: '#0033cc',
    marginVertical: 10,
    height: 50,
  },
  textoMostrar: {
    color: '#FFF',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default App;
