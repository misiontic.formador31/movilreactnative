import React from 'react';
import {Text,StyleSheet,View, Button, TouchableHighlight } from 'react-native';

console.log("entro a citas");


const Cita = ({item, eliminarPaciente}) => {
    
    const dialogoEliminar = id => {
        console.log('eliminando...', id);
        eliminarPaciente(id)
    }
    
    return(
    <View style={styles.cita}>
        <View>
            <Text style={styles.label}>Paciente:</Text>
            <Text style={styles.texto}>{item.pacientes}</Text>
        </View>

        <View>
            <Text style={styles.label}>Propietario:</Text>
            <Text style={styles.texto}>{item.propietario}</Text>
        </View>

        <View>
            <Text style={styles.label}>Síntomas:</Text>
            <Text style={styles.texto}>{item.sintomas}</Text>
        </View>

        <View>
            <TouchableHighlight onPress={()=>dialogoEliminar(item.id)} style={styles.btnEliminar}>
                <Text style={styles.textoEliminar}>Eliminar &times;</Text>
            </TouchableHighlight>
        </View>
    </View>
        
    );
}

const styles = StyleSheet.create({
    cita:{
        backgroundColor:'#FFF',
        borderColor:'#e1e1e1',
        borderStyle:'solid',
        borderBottomWidth:1,
        
    },
    label:{
        fontWeight: 'bold',
        fontSize:18,
        
    },
    texto:{
        fontSize:18,
        color:"#0000ff"
    },
    btnEliminar:{
        padding:10,
        backgroundColor:"#33ccff",
        marginVertical:10
    }, 
    textoEliminar:{
        color:'#FFF',
        fontWeight:'bold',
        textAlign:'center'
    }
})



export default Cita;