import React, {useState} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Alert,
  ScrollView,
} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import shortid from 'shortid';

const Formulario = ({citas, setCitas, guardarMostrarForm}) => {
  //useState para capturar Paciente, propietario, telefono, fecha, hora y sintomas
  const [pacientes, guardarPaciente] = useState('');
  const [propietario, guardarPropietario] = useState('');
  const [telefono, guardarTelefono] = useState('');
  const [fecha, guardarFecha] = useState('');
  const [hora, guardarHora] = useState('');
  const [sintomas, guardarSintomas] = useState('');

  //Visibilidad de fecha y hora
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isTimePickerVisible, setTimePickerVisibility] = useState(false);

  //ocultar y mostrar fecha
  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const confirmarFecha = date => {
    const opciones = {year: 'numeric', month: 'long', day: '2-digit'};
    guardarFecha(date.toLocaleDateString('es-ES', opciones));
    hideDatePicker();
    return;
    console.warn('Una fecha ha sido seleccionada: ', date);
  };

  //ocultar y mostrar hora

  const showTimePicker = () => {
    setTimePickerVisibility(true);
  };

  const hideTimePicker = () => {
    setTimePickerVisibility(false);
  };

  const confirmarHora = time => {
    const opciones = {hour: 'numeric', minute: '2-digit', hour12: true};
    guardarHora(time.toLocaleString('es-US', opciones));
    hideTimePicker();
  };

  //Metodo para crear nueva cita
  const crearNuevaCita = () => {
    console.log('guardando desde nueva cita');

    //Validar campos llenos

    if (
      pacientes.trim() === '' ||
      propietario.trim() === '' ||
      telefono.trim() === '' ||
      fecha.trim() === '' ||
      hora.trim() === '' ||
      sintomas.trim() === ''
    ) {
      //true, es decir algo falló -> algún campo vacio
      console.log('Algo falló, los campos están vacios');
      mostrarAlerta();
      return;
    }

    const cita = {pacientes, propietario, telefono, fecha, hora, sintomas};
    cita.id = shortid.generate();
    const nuevasCitas = [...citas, cita];
    setCitas(nuevasCitas);
    guardarMostrarForm(false);

  };


  const mostrarAlerta = () => {
    Alert.alert(
      '¡Error', //Titulo
      'Todos los campos son obligatorios', //mensaje
      [
        {text: 'OK', onPress: () => console.log('Presiono el ok')},
        {text: 'cancelar'},
      ],
    );
  };

  return (
    <>
      <ScrollView style={styles.formulario}>
        {/*Modificacion de estilos para bloq formulario*/}
        <View>
          <Text style={styles.label}>Paciente:</Text>
          <TextInput
            style={styles.input}
            onChangeText={texto => guardarPaciente(texto)}
          />
        </View>

        <View>
          <Text style={styles.label}>Propietario:</Text>
          <TextInput
            style={styles.input}
            onChangeText={texto => guardarPropietario(texto)}
          />
        </View>

        <View>
          <Text style={styles.label}>Teléfono de Contacto:</Text>
          <TextInput
            style={styles.input}
            onChangeText={texto => guardarTelefono(texto)}
            keyboardType="phone-pad"
          />
        </View>

        <View>
          <Text style={styles.label}>Fecha:</Text>
          <Button title="Selecciona la fecha" onPress={showDatePicker} />
          <DateTimePickerModal
            isVisible={isDatePickerVisible}
            mode="date"
            onConfirm={confirmarFecha}
            onCancel={hideDatePicker}
            locale="es_ES"
            headerTextIOS="Elige una Fecha"
          />
          <Text>{fecha}</Text>
        </View>

        <View>
          <Text style={styles.label}>Hora:</Text>
          <Button title="Selecciona la hora" onPress={showTimePicker} />
          <DateTimePickerModal
            isVisible={isTimePickerVisible}
            mode="time"
            onConfirm={confirmarHora}
            onCancel={hideTimePicker}
            locale="es_ES"
            headerTextIOS="Elige una Hora"
            is24Hour
          />
          <Text>{hora}</Text>
        </View>

        <View>
          <Text style={styles.label}>Sintomas:</Text>
          <TextInput
            multiline
            style={styles.input}
            onChangeText={texto => guardarSintomas(texto)}
          />
        </View>

        <View>
          <TouchableHighlight
            onPress={() => crearNuevaCita()}
            style={styles.btnEliminar}>
            <Text style={styles.textoEliminar}>Guardar</Text>
          </TouchableHighlight>
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  formulario: {
    backgroundColor: '#FFF',
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginHorizontal: '2.5%',
  },
  label: {
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 5,
  },
  input: {
    marginTop: 10,
    height: 50,
    borderColor: '#e1e1e1',
    borderWidth: 1,
    borderStyle: 'solid',
  },
  btnEliminar: {
    padding: 10,
    backgroundColor: '#0033cc',
    marginVertical: 10,
  },
  textoEliminar: {
    color: '#FFF',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
export default Formulario;
