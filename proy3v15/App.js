/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {Text} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import shortid from 'shortid';

const App = () => {

  //Variable durante la ejecución
  const [temp, tempEditable] = useState('');
  const [id, setId] = useState(shortid.generate());

  //Metodo para guardar de forma local
  const storeData = async (value) => {
    try {
      await AsyncStorage.setItem('@storage_Key', value);
    } catch (e) {
      console.log('Error', e);
    }
  };


  //Metodo para leer los datos de forma local
  const getData = async () => {
    try {
      const value = await AsyncStorage.getItem('@storage_Key');
      if (value !== null) {
        tempEditable(value);
      }
    } catch (e) {
      return e;
    }
  };


  //storeData('funciona '); //inicial
  getData();//durante la ejecucion
  //storeData(temp)

  return (
    <>
      <Text>Hola mundo, {temp}</Text>
    </>
  );
};

export default App;
