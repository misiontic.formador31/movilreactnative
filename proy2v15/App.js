/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import {StyleSheet, Image, View, Text, ScrollView} from 'react-native';

const App = () => {
  return (
    <>
      <ScrollView>
        <View style={{flexDirection: 'row'}}>
          <Image style={styles.banner} source={require('./img/bg.jpg')} />
        </View>

        <View style={styles.contenedor}>
          <Text style={styles.titulo}>Qué hacer en Colombia</Text>
          <ScrollView horizontal>
            <View>
              <Image
                style={styles.ciudad}
                source={require('./assets/img/actividad1.jpg')}
              />
            </View>
            <View>
              <Image
                style={styles.ciudad}
                source={require('./img/actividad2.jpg')}
              />
            </View>
            <View>
              <Image
                style={styles.ciudad}
                source={require('./img/actividad3.jpg')}
              />
            </View>
            <View>
              <Image
                style={styles.ciudad}
                source={require('./img/actividad4.jpg')}
              />
            </View>
            <View>
              <Image
                style={styles.ciudad}
                source={require('./img/actividad5.jpg')}
              />
            </View>
          </ScrollView>

          <Text style={styles.titulo}>Los mejores hoteles en Colombia</Text>
          <View>
            <View>
              <Image
                style={styles.mejores}
                source={require('./img/mejores1.jpg')}
              />
            </View>
            <View>
              <Image
                style={styles.mejores}
                source={require('./img/mejores2.jpg')}
              />
            </View>
            <View>
              <Image
                style={styles.mejores}
                source={require('./img/mejores3.jpg')}
              />
            </View>
          </View>
          <Text style={styles.titulo}>Hospedajes en Santander</Text>
          <View style={styles.listado}>
            <View>
              <Image
                style={styles.listadoItem}
                source={require('./img/hospedaje1.jpg')}
              />
            </View>
            <View>
              <Image
                style={styles.listadoItem}
                source={require('./img/hospedaje2.jpg')}
              />
            </View>
            <View>
              <Image
                style={styles.listadoItem}
                source={require('./img/hospedaje3.jpg')}
              />
            </View>
            <View>
              <Image
                style={styles.listadoItem}
                source={require('./img/hospedaje4.jpg')}
              />
            </View>
          </View>
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  banner: {
    height: 250,
    flex: 1,
  },
  titulo: {
    fontWeight: 'bold',
    fontSize: 24,
    marginVertical: 20,
  },
  contenedor: {
    marginHorizontal: 10,
  },
  ciudad: {
    width: 250,
    height: 300,
    marginRight: 10,
  },
  mejores: {
    width: '100%',
    height: 200,
    marginVertical: 5,
  },
  listado:{
    flex:1,
    flexDirection:'row',
    flexWrap:'wrap',
    justifyContent:'space-between'
  },
  listadoItem:{
    
    flexBasis:'49%',
    
  }
});

export default App;
